# Graph-Simulation-3
This project is to generate simulated graph from a given graph. This script has strong dependency on the name of the columns, types of the columns and format of the input file.

This script requires the following softwares:

1. Spark 2.1.0, prebuilt for Hadoop 2.7 and later

2. R 3.2.1 to generate graph properties.

3. Python 2.7.8 is required along with the following packages: openmpi-1.10, numpy 1.10.4, panda 0.18.0, random, csv, gc (garbage collector), sklearn 0.18.1(cluster, KMeans, kneighbors_graph, scale), sys, subprocess. 

To run this project, please follow the guidelines on "How_To_Run_Project.docx" 
	
This script generates a simulated graph named simulated_graph.csv 
